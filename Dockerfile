FROM httpd

WORKDIR /var/www

COPY . /var/www/html
EXPOSE 80
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]
